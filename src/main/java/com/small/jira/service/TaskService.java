package com.small.jira.service;

import com.small.jira.model.Task;
import com.small.jira.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    public List<Task> getAll() {
        return taskRepository.findAll();
    }

    public Optional<Task> findById(Integer id) {
        return taskRepository.findById(id);
    }

    public void createTask(Task task) {
        taskRepository.save(task);
    }

    public Task getTask(Integer id) {
        return taskRepository.getOne(id);
    }

    public void deleteTask(Task task) {
        taskRepository.delete(task);
    }

    public List<Task> getAllCreatedTasks(Integer id) {
        return taskRepository.findAllByCreatedby(id);
    }

    public List<Task> getAllAssignedTasks(Integer id) {
        return taskRepository.findAllByAssignedto(id);
    }
}
