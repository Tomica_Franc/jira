package com.small.jira.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
@PropertySource("classpath:application.properties")
public class NotificationService {

    //value of server response time
    @Value( "${server.timeout.property}" )
    private int timeout;

    //number of max tries for sending notification
    @Value( "${notification.send.tries}" )
    private int tries;

    @Async
    public void sendNotification(String destination) {
        int step = 0;
        boolean status = false;
        while(step != tries) {
            if(status == true) break;
            System.out.println("Sending notification to: " + destination);
            sleep(timeout);
            status = getRandomBoolean();
            System.out.println("Notification send status: " + status + " for destination: " + destination);
            step++;
        }
    }

    private void sleep(int arg) {
        try {
            TimeUnit.SECONDS.sleep(arg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }
}
