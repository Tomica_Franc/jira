package com.small.jira.service;

import com.small.jira.model.User;
import com.small.jira.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public Optional<User> findById(Integer id) {
        return userRepository.findById(id);
    }

    public void createUser(User user) {
        userRepository.save(user);
    }

    public User getUser(Integer id) {
        return userRepository.getOne(id);
    }

    public void deleteUser(User user) {
        userRepository.delete(user);
    }
}
