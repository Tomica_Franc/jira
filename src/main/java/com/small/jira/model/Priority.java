package com.small.jira.model;

public enum Priority {
    LOW, MEDIUM, HIGH, CRITICAL;
}
