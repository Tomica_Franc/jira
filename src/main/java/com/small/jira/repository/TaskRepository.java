package com.small.jira.repository;

import com.small.jira.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository  extends JpaRepository<Task, Integer> {

    List<Task> findAllByCreatedby(Integer id);

    List<Task> findAllByAssignedto(Integer id);
}
