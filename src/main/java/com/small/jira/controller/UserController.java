package com.small.jira.controller;

import com.small.jira.model.User;
import com.small.jira.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/getAll")
    public List<User> getAllUsers() {
        return userService.getAll();
    }

    @GetMapping("/getById/{id}")
    public User getUserById(@PathVariable final Integer id) {
        Optional<User> optional = userService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("User with given id not found!"));
    }

    @PostMapping("/createUser")
    public void createUser(@RequestBody User user) {
        if(user.getFirstname() == null || user.getLastname() == null ||
                user.getPassword() == null || user.getUsername() == null) {
            throw new RuntimeException("User does not have all necessary arguments!");
        } else if(user.getEmail() == null && user.getSms() == null) {
            throw new RuntimeException("User must have SMS number or email!");
        } else {
            userService.createUser(user);
        }
    }

    @PutMapping("/updateUser/{id}")
    public void updateUser(@RequestBody User user, @PathVariable int id) {

        Optional<User> optional = userService.findById(id);
        User currentUser = optional.get();

        if (currentUser != null) {
            if(user.getFirstname() != null) {
                currentUser.setFirstname(user.getFirstname());
            }
            if(user.getLastname() != null) {
                currentUser.setLastname(user.getLastname());
            }
            if(user.getUsername() != null) {
                currentUser.setUsername(user.getUsername());
            }
            if(user.getPassword() != null) {
                currentUser.setPassword(user.getPassword());
            }
            if(user.getSms() != null) {
                currentUser.setSms(user.getSms());
            }
            if(user.getEmail() != null) {
                currentUser.setEmail(user.getEmail());
            }

            userService.createUser(currentUser);
        } else {
            throw new RuntimeException("User does not exist!");
        }
    }

    @GetMapping("/deleteUser/{id}")
    public void deleteUser(@PathVariable final Integer id) {
        User user = userService.getUser(id);

        if(user != null) {
            userService.deleteUser(user);
        }
    }

}
