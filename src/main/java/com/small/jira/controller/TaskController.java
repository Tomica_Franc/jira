package com.small.jira.controller;

import com.small.jira.model.Priority;
import com.small.jira.model.Task;
import com.small.jira.model.User;
import com.small.jira.service.NotificationService;
import com.small.jira.service.TaskService;
import com.small.jira.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    UserService userService;

    @Autowired
    NotificationService notificationService;

    @GetMapping("/getAll")
    public List<Task> getAllTasks() {
        return taskService.getAll();
    }

    @GetMapping("/getById/{id}")
    public Task getTaskById(@PathVariable final Integer id) {
        Optional<Task> optional = taskService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("Task with given id not found!"));
    }

    //All tasks that user created
    @GetMapping("/getAllCreatedTasks/{id}")
    public List<Task> getAllCreatedTasks(@PathVariable final Integer id) {
        return taskService.getAllCreatedTasks(id);
    }

    //All tasks that user got
    @GetMapping("/getAllAssignedTasks/{id}")
    public List<Task> getAllAssignedTasks(@PathVariable final Integer id) {
        return taskService.getAllAssignedTasks(id);
    }

    @PostMapping("/createTask")
    public void createTask(@RequestBody Task task) {
        if(task.getName() == null || task.getDescription() == null || task.getPriority() == null
                || task.getCreatedby() == 0 || task.getAssignedto() == 0) {
            throw new RuntimeException("Task does not have all necessary arguments!");
        } if(userService.getUser(task.getCreatedby()) == null || userService.getUser(task.getAssignedto()) == null) {
            throw new RuntimeException("Some users does not exist!");
        } else {
            task.setPriority(Priority.valueOf(task.getPriority().toString()));
            taskService.createTask(task);
            prepareNotification(task); //send notification for new task
        }
    }

    private void prepareNotification(Task task) {
        if(task != null) {
            if(task.getAssignedto() != 0) {
                User user = userService.getUser(task.getAssignedto());
                if(user != null) {
                    if(user.getEmail() != null) {
                        //for real world methods should be different for sending email and sms
                        notificationService.sendNotification(user.getEmail());
                    } else if(user.getSms() != null) {
                        notificationService.sendNotification(user.getSms());
                    } else {
                        throw new RuntimeException("There is no contact for notification!");
                    }
                } else {
                    throw new RuntimeException("User does not exist!");
                }
            }
        }
    }

    @PutMapping("/updateTask/{id}")
    public void updateTask(@RequestBody Task task, @PathVariable int id) {

        Optional<Task> optional = taskService.findById(id);
        Task currentTask = optional.get();

        if (currentTask != null) {
            if(task.getName() != null) {
                currentTask.setName(task.getName());
            }
            if(task.getDescription() != null) {
                currentTask.setDescription(task.getDescription());
            }
            if(task.getPriority() != null) {
                currentTask.setPriority(Priority.valueOf(task.getPriority().toString()));
            }
            if(task.getCreatedby() != 0) {
                if(userService.getUser(task.getCreatedby()) == null) {
                    throw new RuntimeException("Task creator does not exist!");
                }
                currentTask.setCreatedby(task.getCreatedby());
            }
            if(task.getAssignedto() != 0) {
                if(userService.getUser(task.getAssignedto()) == null) {
                    throw new RuntimeException("Task assignee does not exist!");
                }
                currentTask.setAssignedto(task.getAssignedto());
            }
            if(task.getDatecreated() != null) {
                currentTask.setDatecreated(task.getDatecreated());
            }

            taskService.createTask(currentTask);
            prepareNotification(currentTask); //send notification when task is updated
        } else {
            throw new RuntimeException("Task does not exist!");
        }
    }

    @GetMapping("/deleteTask/{id}")
    public void deleteTask(@PathVariable final Integer id) {
        Task task = taskService.getTask(id);

        if(task != null) {
            taskService.deleteTask(task);
        }
    }

}
